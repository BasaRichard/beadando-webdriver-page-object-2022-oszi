﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace WebDriverClass
{
    class BadandoPageObjectDataTest : TestBase
    {
        [Test, TestCaseSource("TestData")]
        public void XMLTest(String link, String search)
        {
            // max hogy müködjön 
            Driver.Manage().Window.Maximize();
            
            Driver.Navigate().GoToUrl(link);
            Thread.Sleep(1000); // kell mert voltr hogy hibára fut lassu a gép 
            IWebElement searchField = Driver.FindElement(By.CssSelector("[class='form-control svelte-1qk8wlx']"));
            searchField.SendKeys(search);

            Assert.AreEqual(search, searchField.GetAttribute("value"));
            Thread.Sleep(1000);// itt valszeg az oldal lassu 
            new Actions(Driver).KeyDown(Keys.Enter).Perform();

        }

        static IEnumerable TestData()
        {
            var doc = XElement.Load(System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory) + "\\data.xml");
            return
                from vars in doc.Descendants("testData")
                let link = vars.Attribute("link").Value
                let search = vars.Attribute("search").Value
                select new object[] { link, search };
        }
    }
}
