﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebDriverClass.WidgetsAtClass;

namespace WebDriverClass.PageAtClass
{
    class SearchPage : BasePage
    {
        public SearchPage(IWebDriver webDriver) : base(webDriver)
        {
        }

        public static SearchPage Navigate(IWebDriver webDriver)
        {
            webDriver.Navigate().GoToUrl("https://nwdb.info/build");
            return new SearchPage(webDriver);
        }
        public static SearchPage Navigate(IWebDriver webDriver, string newSite = "")
        {
            if (newSite == "")
            {
                webDriver.Navigate().GoToUrl("https://nwdb.info/build");
                return new SearchPage(webDriver);
            }
            else
            {
                webDriver.Navigate().GoToUrl(newSite);
                return new SearchPage(webDriver);
            }

        }

        public SearchDatabaseWidget GetSearchDatabaseWidge()
        {
            return new SearchDatabaseWidget(Driver);
        }

        public WeaponTypeResultWidget GetWeaponTypeResultWidget()
        {
            return new WeaponTypeResultWidget(Driver);
        }

        public ExperienceTableSearchWidget GetExperienceTableSearchWidget()
        {
            return new ExperienceTableSearchWidget(Driver);
        }
        public ServerStatusCheckWidget GetServerStatusCheckWidget()
        {
            return new ServerStatusCheckWidget(Driver);
        }
    }
}
