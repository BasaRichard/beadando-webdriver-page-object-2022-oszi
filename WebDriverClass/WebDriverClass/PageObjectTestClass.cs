﻿using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebDriverClass.PageAtClass;
using WebDriverClass.WidgetsAtClass;

namespace WebDriverClass
{
    
    class PageObjectTestClass : TestBase
    {
        [Test]
        public void NumberofWeaponTypeTest()
        {
            WeaponTypeResultWidget resultWidget = SearchPage.Navigate(Driver).GetWeaponTypeResultWidget();

            int a = resultWidget.WeaponCountResoults();
            Console.WriteLine(a);
            //yeah 14 fajta fegyver van 
            Assert.AreEqual(a.ToString(), "14");
            //SearchPage.Navigate(Driver, "https://www.youtube.com/watch?v=EgVt4oiAHqM").GetWeaponTypeResultWidget();
        }
        [Test]
        public void SideBarMainElementsCountTest()
        {
            SearchDatabaseWidget searchDatabaseWidget = SearchPage.Navigate(Driver, "https://nwdb.info/db/items/page/1").GetSearchDatabaseWidge();

            int a = searchDatabaseWidget.SideBarMainElementsCount();

            Assert.AreEqual(a.ToString(), "12");

        }
        [Test]
        public void UpBarMainElementsCountTest()
        {
            SearchDatabaseWidget searchDatabaseWidget = SearchPage.Navigate(Driver, "https://nwdb.info/db/items/page/1").GetSearchDatabaseWidge();

            int a = searchDatabaseWidget.SideBarMainElementsCount();

            //Assert.AreEqual(a.ToString(), "12");
            searchDatabaseWidget.GetListAcquisitionGoGItems();

        }

        [Test]
        public void SearchListOfGoGItemsTest()
        {
            SearchDatabaseWidget searchDatabaseWidget = SearchPage.Navigate(Driver, "https://nwdb.info/db/items/page/1").GetSearchDatabaseWidge();

            searchDatabaseWidget.SearchListOfGoGItems();

            Assert.AreEqual( "https://nwdb.info/db/items/page/1?source=expedition_genesis", Driver.Url);
             //searchDatabaseWidget.GetListAcquisitionGoGItems();

        }
        [Test]
        public void CheckTheTotalUmbraTest()
        {
            ExperienceTableSearchWidget experienceTableSearchWidget = SearchPage.Navigate(Driver, "https://nwdb.info/experience-table/character-level").GetExperienceTableSearchWidget();

            string number = experienceTableSearchWidget.ChehckTheTotalUmbral();

            Assert.AreEqual(number, "9,440");
            

        }
        [Test]
        public void CheckMyServerStatusTest()
        {
            ServerStatusCheckWidget serverStatusCheckWidget = SearchPage.Navigate(Driver, "https://nwdb.info/server-status").GetServerStatusCheckWidget();

            string serverName = serverStatusCheckWidget.ChackMyServerStatus();

            Assert.AreEqual(serverName, "Tir Na Nog");

        }


    }
}
