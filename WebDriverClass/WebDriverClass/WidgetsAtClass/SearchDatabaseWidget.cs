﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WebDriverClass.PageAtClass;

namespace WebDriverClass.WidgetsAtClass
{
    class SearchDatabaseWidget : BasePage
    {
        public SearchDatabaseWidget(IWebDriver driver) : base(driver)
        {
             wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(20));
        }
        private WebDriverWait wait;
        private IWebElement SideBar => wait.Until(d => d.FindElement(By.CssSelector("[class='bd-sidebar']")));
        private IWebElement Acquisition => wait.Until(d => d.FindElement(By.XPath("//*[@id='svelte']/div[3]/div/main/div[3]")));
        private IWebElement GoGButton => wait.Until(d => d.FindElement(By.XPath("//*[@id='svelte']/div[3]/div/main/div[3]/div/div/button[16]")));
        private List<IWebElement> TopBar => SideBar.FindElements(By.XPath("//*[@id='svelte']/div[3]/div/main/div[contains(@class, 'item-filters mb-2 d-inline-block')]")).ToList();

        private List<IWebElement> SideBarElements => SideBar.FindElements(By.XPath("*[@id='bd-docs-nav']/ul/li")).ToList();

        public int SideBarMainElementsCount()
        {
            return (SideBarElements.Count());
        }

        //Garden of Genesis
        public void GetListAcquisitionGoGItems()
        {
            foreach (var item in TopBar)
            {
                Console.WriteLine(item.Text);
                Console.WriteLine(item.Location);
                Console.WriteLine("");
            }
        }
        public void  SearchListOfGoGItems()
        {
            //WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(10));
            //wait.Until(d => d.FindElement(By.XPath("//*[@id='svelte']/div[4]/div/main/div[3]/div/button")));
            //Thread.Sleep(1000);
            Acquisition.Click();
           // Thread.Sleep(1000);
            GoGButton.Click();
            Thread.Sleep(1000);
            Driver.Navigate().Refresh();


        }
        //WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(10));
        //Driver.Manage()
        //      .Timeouts().SetPageLoadTimeout(TimeSpan.FromSeconds(30));
        //Acquisition.Click();
        //    //*[@id="svelte"]/div[4]/div/main/div[3]/div/button
        //     wait.Until(d => d.FindElement(By.XPath("//*[@id='svelte']/div[4]/div/main/div[3]/div/button")).);
        //  //  wait.Until(d => d.FindElement(By.XPath("//*[@id='svelte']/div[3]/div/main/div[3]/div/div/button[16]")));v
       
    }
}

