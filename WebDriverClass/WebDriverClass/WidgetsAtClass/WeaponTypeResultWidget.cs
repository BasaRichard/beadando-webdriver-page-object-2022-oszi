﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebDriverClass.PageAtClass;


namespace WebDriverClass.WidgetsAtClass
{
    class WeaponTypeResultWidget : BasePage
    {
        public WeaponTypeResultWidget(IWebDriver driver) : base(driver)
        { }
        private IWebElement Timetable => Driver.FindElement(By.CssSelector("[class='panel mb-2 p-2 text-center']"));

        private List<IWebElement> Lines => Timetable.FindElements(By.XPath("//div/div/button/span[.='0']")).ToList();


        public int WeaponCountResoults()
        {
            return (Lines.Count());
        }
        public List<IWebElement> WeaponResoults()
        {
            return (Lines);
        }


    }
}