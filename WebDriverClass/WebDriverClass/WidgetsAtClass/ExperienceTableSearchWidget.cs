﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WebDriverClass.PageAtClass;

namespace WebDriverClass.WidgetsAtClass
{
    class ExperienceTableSearchWidget : BasePage
    {
        public ExperienceTableSearchWidget(IWebDriver driver) : base(driver)
        {
            
        }
        private IWebElement highestLvL => Driver.FindElement(By.XPath("//*[@id='expTable']/tbody/tr[59]/td[1]"));
        private IWebElement umbralShards => Driver.FindElement(By.XPath("//*[@id='svelte']/div[3]/div[2]/div/div/div/a[3]"));
        private IWebElement searchButton => Driver.FindElement(By.XPath("//*[@id='svelte']/div[3]/div[2]/div"));
        private IWebElement totaltUmbral => Driver.FindElement(By.XPath(" //*[@id='expTable']/tbody/tr[36]/td[2]"));
        public string ChehckTheTotalUmbral()
        {
            ChangeToListUmbralShards();
          
             IWebElement totaltUmbral = Driver.FindElement(By.XPath(" //*[@id='expTable']/tbody/tr[36]/td[2]"));
            return totaltUmbral.Text; 
        }
        public void ChangeToListUmbralShards()
        {
           
            searchButton.Click();
         
            umbralShards.Click();

        }

    }
}
