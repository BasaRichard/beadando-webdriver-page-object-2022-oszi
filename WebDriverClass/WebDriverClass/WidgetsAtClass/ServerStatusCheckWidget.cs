﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WebDriverClass.PageAtClass;

namespace WebDriverClass.WidgetsAtClass
{
    class ServerStatusCheckWidget : BasePage
    {
        public ServerStatusCheckWidget(IWebDriver driver) : base(driver)
        {
            wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(20));
        }
        private WebDriverWait wait;

        private IWebElement setRegion => wait.Until(d => d.FindElement(By.XPath("//*[@id='svelte']/div[3]/div[5]/div/div[1]/div/div[2]")));
        private IWebElement searchButton => wait.Until(d => d.FindElement(By.XPath("//*[@id='svelte']/div[3]/div[5]/div/div[1]/div/div[2]/div/button[1]")));
        private IWebElement onlinePeople => wait.Until(d => d.FindElement(By.XPath("//*[@id='svelte']/div[3]/div[5]/div/div[2]/div/section/article/table/thead/tr/th[2]")));

        public string ChackMyServerStatus()
        {
            setRegion.Click();
            searchButton.Click();
            onlinePeople.Click();
            string lastNamed = Driver.FindElement(By.XPath("//*[@id='svelte']/div[3]/div[5]/div/div[2]/div/section/article/table/tbody/tr[1]/td[2]/div/span[2]")).Text;
            Console.WriteLine(lastNamed); //Tir Na Nog
            return (lastNamed);
        }
    }
}
