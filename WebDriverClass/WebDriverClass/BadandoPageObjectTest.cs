﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using WebDriverClass;


namespace WebDriverClass
{
    class BadandoPageObjectTest : TestBase
    {
        private String assertTitleChange(String oldTitle)
        {
            String newTitle = Driver.Title;
            Assert.AreNotEqual(oldTitle, newTitle);
            return newTitle;
        }

        [Test]
        public void NavigationExample()
        {
            String title = "";

            //Navigate to https://nwdb.info/
            Driver.Navigate().GoToUrl(new Uri("https://nwdb.info/"));
            title = assertTitleChange(title);

            Assert.AreEqual(title, Driver.Title);
            Assert.AreEqual("https://nwdb.info/", Driver.Url); 
        }
        [Test]
        public void ClickExample()
        {
            
            Driver.Navigate().GoToUrl(new Uri("https://nwdb.info/"));
            Driver.Manage().Window.Maximize();
            Driver.FindElement(By.CssSelector("a[class='btn btn-secondary d-flex align-items-center svelte-p98kqx']")).Click();
            Driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(5);
            var wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(4));
            wait.Until(ExpectedConditions.ElementToBeClickable(By.CssSelector("a[class='btn btn-secondary d-flex align-items-center svelte-p98kqx']")));
            Thread.Sleep(1000); // kell a várakozás különben hibára fut it tnem tudom miért csak ugy müködik hogy ha tehad.slepelek
            StringAssert.Contains("Home - New World Database", Driver.Title);
            Assert.AreEqual("https://ptr.nwdb.info/", Driver.Url);
           
            
        }
        [Test]
        public void CreateBuild()
        {
            Driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
            Driver.Navigate().GoToUrl(new Uri("https://nwdb.info/"));
            Driver.Manage().Window.Maximize();
            Driver.FindElement(By.LinkText("Skill Builder")).Click();
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(10));
            wait.Until(d => d.FindElement(By.Id("skill-tooltip-ability_greatsword_combo")).Displayed);
            //Thread.Sleep(1000);
            Driver.FindElement(By.Id("skill-tooltip-ability_greatsword_combo")).Click();
            Driver.FindElement(By.Id("skill-tooltip-passive_greatsword_off_2")).Click();
            Driver.FindElement(By.Id("skill-tooltip-passive_greatsword_off_1")).Click();
            //Thread.Sleep(1000);
           
            var a = Driver.FindElements(By.XPath("//h2//span[.='3']"));
            Assert.AreEqual(a.Count(), 1);

        }

        [Test]
        public void ChangeLanguage()
        {
            Driver.Navigate().GoToUrl("https://nwdb.info/");
            Driver.Manage().Window.Maximize();
            Driver.FindElement(By.CssSelector("[class='dropdown-toggle btn btn-secondary']")).Click();
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(10));
            wait.Until(d => d.FindElement(By.XPath("//span[.='Polski']")).Displayed);
           // Thread.Sleep(2000);
            
            Driver.FindElement(By.XPath("//span[.='Polski']")).Click();

        }
        [Test]
        public void FindItem()
        {
            //Driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
            Driver.Navigate().GoToUrl("https://nwdb.info/");
            Driver.Manage().Window.Maximize();
           // Thread.Sleep(1000);// kell a várakozás különben hibára fut 
            //WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(10));
            //wait.Until(d => d.FindElement(By.CssSelector("[class='form-control svelte-1qk8wlx']")).Displayed);
            IWebElement searchField = Driver.FindElement(By.CssSelector("[class='form-control svelte-1qk8wlx']"));
            searchField.SendKeys("Will of the Ancients");
            Assert.AreEqual("Will of the Ancients", searchField.GetAttribute("value"));
            Thread.Sleep(1000);// kell a várakozás különben hibára fut valamiért hiába probáltam más várakoztatásal nem tud entert nyomni
            new Actions(Driver).KeyDown(Keys.Enter).Perform();
            //wait.Until(d => d.FindElement(By.CssSelector("[class='form-control svelte-1qk8wlx']")).Displayed);

            //check the URL
            Assert.AreEqual("https://nwdb.info/db/item/2hstafflife_willoftheancientst5_v2", Driver.Url); 
        }



    }
}
